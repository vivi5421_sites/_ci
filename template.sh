#! /bin/sh

DESTINATION_BASEDIR=$1
SITE=$2
VENV=$3

cat << EOF
[uwsgi]
#application's base folder
base = $DESTINATION_BASEDIR/$SITE
plugin = python3

#python module to import
app = $SITE
module = %(app)

home = %(base)/$VENV
pythonpath = %(base)

#socket file's location
socket = %(base)/%n.sock

#permissions for the socket file
chmod-socket = 565

#the variable that holds a flask application inside the module imported at line #6
callable = app

#location of log files
logto = /var/log/uwsgi/%n.log

limit-as = 2056

# Allow threads created in the application
enable-threads = true
EOF