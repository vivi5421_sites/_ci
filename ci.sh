#! /bin/bash

set -x

HOST="home.villard.me"
HOST_BASEDIR="/home/sites"
VENV=".venv"

apt-get update -qy

apt-get install -qy rsync sshpass tree

./_ci/template.sh $HOST_BASEDIR $SITE $VENV > "$SITE.ini"

cat "$SITE.ini"

tree -apughD -I .git $* .

rsync -avzh --exclude=.git* --exclude=_ci --exclude=__pycache__ --exclude="$VENV" --perms --force --delete --delete-excluded --rsh="sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -l $USERNAME" . "$HOST:$HOST_BASEDIR/$SITE"

sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no $USERNAME@$HOST << EOF
 echo '-> +++ virtualenv -p python3 "$HOST_BASEDIR/$SITE/$VENV"'
 virtualenv -p python3 "$HOST_BASEDIR/$SITE/$VENV"
 echo '-> +++ "$HOST_BASEDIR/$SITE/$VENV/bin/python" -m pip install -r "$HOST_BASEDIR/$SITE/requirements.txt"'
 "$HOST_BASEDIR/$SITE/$VENV/bin/python" -m pip install -r "$HOST_BASEDIR/$SITE/requirements.txt"
EOF
